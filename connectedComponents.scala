package org.apache.spark.examples.graphx
import org.apache.spark.graphx.GraphLoader
import org.apache.spark.sql.SparkSession

val sc = new SparkContext("local", "Connected Components", new SparkConf())

val graph = GraphLoader.edgeListFile(sc, "../data/graphx/followers.txt")
val cc = graph.connectedComponents().vertices
val users = sc.textFile("../data/graphx/users.txt").map { line =>
  val fields = line.split(",")
  (fields(0).toLong, fields(1))
}
val ccByUsername = users.join(cc).map {
  case (id, (username, cc)) => (username, cc)
}
println(ccByUsername.collect().mkString("\n"))

sc.stop()
System.exit(0)